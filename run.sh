cd demo && mvn clean install

java -javaagent:elastic-apm-agent-1.36.0.jar -Delastic.apm.attach=true -Delastic.apm.server_url=http://localhost:8200 -Delastic.apm.service_name=demo-application -Delastic.apm.application_packages=com.example -Delastic.apm.environment=production -jar target/demo-0.0.1-SNAPSHOT.jar